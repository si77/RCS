<?php

/**
 * @file StatisticsHandler.inc.php
 *
 * Copyright (c) 2003-2011 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class StatisticsHandler
 * @ingroup pages_manager
 *
 * @brief Handle requests for statistics functions.
 */

// $Id$

import('pages.manager.ManagerHandler');

class StatisticsHandler extends ManagerHandler {
	/**
	 * Constructor
	 **/
	function StatisticsHandler() {
		parent::ManagerHandler();
	}
	/**
	 * Display a list of journal statistics.
	 * WARNING: This implementation should be kept roughly synchronized
	 * with the reader's statistics view in the About pages.
	 */
	function statistics() {
		$this->validate();
		$this->setupTemplate(true);

		$journal =& Request::getJournal();
		$templateMgr =& TemplateManager::getManager();

		$statisticsYear = Request::getUserVar('statisticsYear');
		if (empty($statisticsYear)) $statisticsYear = date('Y');
		$templateMgr->assign('statisticsYear', $statisticsYear);

		$sectionIds = $journal->getSetting('statisticsSectionIds');
		if (!is_array($sectionIds)) $sectionIds = array();
		$templateMgr->assign('sectionIds', $sectionIds);

		foreach ($this->getPublicStatisticsNames() as $name) {
			$templateMgr->assign($name, $journal->getSetting($name));
		}
		$templateMgr->assign('statViews', $journal->getSetting('statViews'));

		$dateType = "subDate";
		if(Request::getUserVar("dateType") == "expDate") {
			$dateType = "expDate";
		}

		$templateMgr->assign('dateType', $dateType);


		// if we have the new date selectors - use those dates
		$startDate = Request::getUserVar('startDate');
		if($startDate) {
			$fromDate = strtotime($startDate);
		} else {
			$fromDate = mktime(0, 0, 0, 1, 1, $statisticsYear);
		}



		$templateMgr->assign('startDate', $fromDate);

		// if we have the new date selectors - use those dates
		$endDate = Request::getUserVar('endDate');
		if($endDate) {
			$toDate = strtotime($endDate);
		} else {
			$toDate = mktime(23, 59, 59, 12, 31, $statisticsYear);
		}

		$templateMgr->assign('endDate', $toDate);

		$journalStatisticsDao =& DAORegistry::getDAO('JournalStatisticsDAO');
		$articleStatistics = $journalStatisticsDao->getArticleStatistics($journal->getId(), null, $fromDate, $toDate, $dateType);
		$templateMgr->assign('articleStatistics', $articleStatistics);

		$limitedArticleStatistics = $journalStatisticsDao->getArticleStatistics($journal->getId(), $sectionIds, $fromDate, $toDate, $dateType);
		$templateMgr->assign('limitedArticleStatistics', $limitedArticleStatistics);

		$sectionDao =& DAORegistry::getDAO('SectionDAO');
		$sections =& $sectionDao->getJournalSections($journal->getId());
		$templateMgr->assign('sections', $sections->toArray());

		$issueStatistics = $journalStatisticsDao->getIssueStatistics($journal->getId(), $fromDate, $toDate, $dateType);
		$templateMgr->assign('issueStatistics', $issueStatistics);

		$reviewerStatistics = $journalStatisticsDao->getReviewerStatistics($journal->getId(), $sectionIds, $fromDate, $toDate, $dateType);
		$templateMgr->assign('reviewerStatistics', $reviewerStatistics);

		$allUserStatistics = $journalStatisticsDao->getUserStatistics($journal->getId(), null, $toDate);
		$templateMgr->assign('allUserStatistics', $allUserStatistics);

		$userStatistics = $journalStatisticsDao->getUserStatistics($journal->getId(), $fromDate, $toDate);
		$templateMgr->assign('userStatistics', $userStatistics);

		if ($journal->getSetting('publishingMode') == PUBLISHING_MODE_SUBSCRIPTION) {
			$allSubscriptionStatistics = $journalStatisticsDao->getSubscriptionStatistics($journal->getId(), null, $toDate, $dateType);
			$templateMgr->assign('allSubscriptionStatistics', $allSubscriptionStatistics);

			$subscriptionStatistics = $journalStatisticsDao->getSubscriptionStatistics($journal->getId(), $fromDate, $toDate, $dateType);
			$templateMgr->assign('subscriptionStatistics', $subscriptionStatistics);
		}

		$reportPlugins =& PluginRegistry::loadCategory('reports');
		$templateMgr->assign_by_ref('reportPlugins', $reportPlugins);

		$templateMgr->assign('helpTopicId', 'journal.managementPages.statsAndReports');

//print_r($limitedArticleStatistics);
//print_r($articleStatistics);
		$totalInCatPercent = round( $limitedArticleStatistics["numSubmissions"] * 100 / $articleStatistics["numSubmissions"]);

		$templateMgr->assign('totalInCatPercent', $totalInCatPercent);

		$csvArgs = array_merge($_GET,array("csv"=>1));
		$templateMgr->assign('csvArgs', http_build_query($csvArgs));


		// custom override for csv
		if(Request::getUserVar("csv")) {
			// inital data set
			$newData = array(
				"Total submissions" => $articleStatistics["numSubmissions"],
				"Total In Category" => $limitedArticleStatistics["numSubmissions"]." (".$totalInCatPercent."%)",
				"Unassigned" => $limitedArticleStatistics["statUnassigned"]." (".$limitedArticleStatistics["statUnassignedPercent"]."%)",
				"Pending" => $limitedArticleStatistics["statPending"]." (".$limitedArticleStatistics["statPendingPercent"]."%)",
				"Peer Reviewed" => $limitedArticleStatistics["numReviewedSubmissions"]." (".$limitedArticleStatistics["numReviewedSubmissionsPercent"]."%)",
				" - Accept" => $limitedArticleStatistics["submissionsAccept"]." (".$limitedArticleStatistics["submissionsAcceptPercent"]."%)",
				" - Decline" => $limitedArticleStatistics["submissionsDecline"]." (".$limitedArticleStatistics["submissionsDeclinePercent"]."%)",
				" - In revision" => $limitedArticleStatistics["submissionsRevise"]." (".$limitedArticleStatistics["submissionsRevisePercent"]."%)",
				" - Days to review" => round($reviewerStatistics["daysPerReview"]),
				" - Days to review (No Revision)" => round($reviewerStatistics["daysPerReviewNoRevise"]),
				"Days to published from submission online" => $limitedArticleStatistics["daysToPublication"],
				" - Online" => $limitedArticleStatistics["statDaysToSubmissionOnline"],
				" - Print" => $limitedArticleStatistics["statDaysToSubmissionPrint"],
				"Days to published from acceptance" => $limitedArticleStatistics["statDaysFromAcceptanceHeading"],
				" - Online" => $limitedArticleStatistics["statDaysFromAcceptanceOnline"],
				" - Print" => $limitedArticleStatistics["statDaysFromAcceptancePrint"]
			);

			header( 'Content-Type: text/csv' );
            header( 'Content-Disposition: attachment;filename=statistics.csv');

			$fp = fopen('php://output', 'w');

			fputcsv($fp, array("From Date:",date("d/m/Y",$fromDate)));
			fputcsv($fp, array("To Date:",date("d/m/Y",$toDate)));


			foreach($newData as $k => $v) {
				fputcsv($fp, array($k,$v));
			}

		}


		$templateMgr->display('manager/statistics/index.tpl');

	}

	function saveStatisticsSections() {
		// The manager wants to save the list of sections used to
		// generate statistics.

		$this->validate();

		$journal =& Request::getJournal();

		$sectionIds = Request::getUserVar('sectionIds');
		if (!is_array($sectionIds)) {
			if (empty($sectionIds)) $sectionIds = array();
			else $sectionIds = array($sectionIds);
		}

		// custom option allowing for all sections to be included in stats
		if($sectionIds[0] == -1) {
			$sectionIds = Request::getUserVar('allSectionIds');
		}

		$journal->updateSetting('statisticsSectionIds', $sectionIds);
		Request::redirect(null, null, 'statistics', null, array('statisticsYear' => Request::getUserVar('statisticsYear')));
	}

	function getPublicStatisticsNames() {
		return array(
			'statNumPublishedIssues',
			'statItemsPublished',
			'statNumSubmissions',
			'statPeerReviewed',
			'statCountAccept',
			'statCountDecline',
			'statCountRevise',
			'statDaysPerReview',
			'statDaysPerReviewNoRevise',
			'statDaysToPublication',
			'statRegisteredUsers',
			'statRegisteredReaders',
			'statSubscriptions',
			'statItemsInCat',
			'statUnassigned',
			'statPending',
			'statDaysToSubmissionHeading',
			'statDaysToSubmissionOnline',
			'statDaysToSubmissionPrint',
			'statDaysFromAcceptanceHeading',
			'statDaysFromAcceptanceOnline',
			'statDaysFromAcceptancePrint',
		);
	}

	function savePublicStatisticsList() {
		$this->validate();

		$journal =& Request::getJournal();
		foreach ($this->getPublicStatisticsNames() as $name) {
			$journal->updateSetting($name, Request::getUserVar($name)?true:false);
		}
		$journal->updateSetting('statViews', Request::getUserVar('statViews')?true:false);
		Request::redirect(null, null, 'statistics', null, array('statisticsYear' => Request::getUserVar('statisticsYear')
																, 'startDate' => Request::getUserVar('startDate')
																, 'endDate' => Request::getUserVar('endDate')
																, 'dateType' => Request::getUserVar('dateType')
																));
	}

	function report($args) {
		$this->validate();
		$this->setupTemplate();

		$journal =& Request::getJournal();

		$pluginName = array_shift($args);
		$reportPlugins =& PluginRegistry::loadCategory('reports');

		if ($pluginName == '' || !isset($reportPlugins[$pluginName])) {
			Request::redirect(null, null, 'statistics');
		}

		$plugin =& $reportPlugins[$pluginName];
		$plugin->display($args);
	}
}

?>
