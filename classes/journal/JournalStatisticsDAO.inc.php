<?php

/**
 * @file classes/journal/JournalStatisticsDAO.inc.php
 *
 * Copyright (c) 2003-2011 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class JournalStatisticsDAO
 * @ingroup journal
 *
 * @brief Operations for retrieving journal statistics.
 */

// $Id$


define('REPORT_TYPE_JOURNAL',	0x00001);
define('REPORT_TYPE_EDITOR',	0x00002);
define('REPORT_TYPE_REVIEWER',	0x00003);
define('REPORT_TYPE_SECTION',	0x00004);

class JournalStatisticsDAO extends DAO {
	/**
	 * Get statistics about articles in the system.
	 * Returns a map of name => value pairs.
	 * @param $journalId int The journal to fetch statistics for
	 * @param $sectionId int The section to query stats for (optional)
	 * @param $dateStart date The submit date to search from; optional
	 * @param $dateEnd date The submit date to search to; optional
	 * @return array
	 */
	function getArticleStatistics($journalId, $sectionIds = null, $dateStart = null, $dateEnd = null,$dateType = "subDate") {
		// Bring in status constants
		import('classes.article.Article');

		$params = array($journalId);
		if (!empty($sectionIds)) {
			$sectionSql = ' AND (a.section_id = ?';
			$params[] = array_shift($sectionIds);
			foreach ($sectionIds as $sectionId) {
				$sectionSql .= ' OR a.section_id = ?';
				$params[] = $sectionId;
			}
			$sectionSql .= ')';
		} else $sectionSql = '';

		$sql =	'SELECT	a.article_id,
				a.date_submitted,
				pa.date_published,
				pa.pub_id,
				d.decision,
				d.date_decided,
				a.status,
				ea.edit_id,
				a.publish_online,
				a.publish_print

			FROM	articles a
				LEFT JOIN published_articles pa ON (a.article_id = pa.article_id)
				LEFT JOIN edit_decisions d ON (d.article_id = a.article_id)
				LEFT JOIN edit_assignments ea ON (a.article_id = ea.article_id)

			WHERE	a.journal_id = ?';

			if($dateType == "subDate") {
				$sql .= ($dateStart !== null ? ' AND a.date_submitted >= ' . $this->datetimeToDB($dateStart) : '') .
						($dateEnd !== null ? ' AND a.date_submitted <= ' . $this->datetimeToDB($dateEnd) : '');
			} else {
				$sql .= ($dateStart !== null ? ' AND d.date_decided >= ' . $this->datetimeToDB($dateStart) : '') .
						($dateEnd !== null ? ' AND d.date_decided <= ' . $this->datetimeToDB($dateEnd) : '');
			}

			$sql .= $sectionSql .
			' ORDER BY a.article_id, d.date_decided DESC';

		$result =& $this->retrieve($sql, $params);

		$returner = array(
			'numSubmissions' => 0,
			'numReviewedSubmissions' => 0,
			'numPublishedSubmissions' => 0,
			'submissionsAccept' => 0,
			'submissionsDecline' => 0,
			'submissionsRevise' => 0,
			'submissionsAcceptPercent' => 0,
			'submissionsDeclinePercent' => 0,
			'submissionsRevisePercent' => 0,
			'daysToPublication' => 0,
			'statUnassigned' => 0,
			'statPending' => 0,
			'statDaysToSubmissionOnline' => 0,
			'statDaysToSubmissionPrint' => 0,
			'statDaysFromAcceptanceOnline' => 0,
			'statDaysFromAcceptancePrint' => 0,

		);

		// Track which articles we're including
		$articleIds = array();

		$totalTimeToPublication = 0;
		$timeToPublicationCount = 0;

		while (!$result->EOF) {
			$row = $result->GetRowAssoc(false);
			// For each article, pick the most recent editor
			// decision only and ignore the rest. Depends on sort
			// order. FIXME -- there must be a better way of doing
			// this that's database independent.
			if (!in_array($row['article_id'], $articleIds)) {
				$articleIds[] = $row['article_id'];
				$returner['numSubmissions']++;

				if (!empty($row['pub_id']) && $row['status'] == STATUS_PUBLISHED) {
					$returner['numPublishedSubmissions']++;
				}

				if (!empty($row['date_submitted']) && !empty($row['date_published']) && $row['status'] == STATUS_PUBLISHED) {
					$timeSubmitted = strtotime($this->datetimeFromDB($row['date_submitted']));
					$timePublished = strtotime($this->datetimeFromDB($row['date_published']));
					if ($timePublished > $timeSubmitted) {
						$totalTimeToPublication += ($timePublished - $timeSubmitted);
						$timeToPublicationCount++;
					}
				}

				import('classes.submission.common.Action');
				switch ($row['decision']) {
					case SUBMISSION_EDITOR_DECISION_ACCEPT:
						$returner['submissionsAccept']++;
						$returner['numReviewedSubmissions']++;
						break;
					case SUBMISSION_EDITOR_DECISION_PENDING_REVISIONS:
					case SUBMISSION_EDITOR_DECISION_RESUBMIT:
						$returner['submissionsRevise']++;
						break;
					case SUBMISSION_EDITOR_DECISION_DECLINE:
						$returner['submissionsDecline']++;
						$returner['numReviewedSubmissions']++;
						break;
				}
				switch ($row['decision']) {
					case SUBMISSION_EDITOR_DECISION_ACCEPT:
					case SUBMISSION_EDITOR_DECISION_DECLINE:
						break;
					default:
						$returner['statPending']++;
						break;
				}
				// statUnassigned, status == queued and edit_id = null
				if($row['status'] == STATUS_QUEUED && !$row['edit_id']){
					$returner['statUnassigned']++;
				}

				if($row['publish_online'] && $row['publish_online'] != "0000-00-00 00:00:00") {
					$timePublish_online = strtotime($this->datetimeFromDB($row['publish_online']));
					$timeSubmitted = strtotime($this->datetimeFromDB($row['date_submitted']));
					$completedOnlineCount++;
					$totalElapsedTimeOnline += ceil(($timePublish_online - $timeSubmitted) / 60 / 60 / 24);
				}

				if($row['publish_print'] && $row['publish_print'] != "0000-00-00 00:00:00") {
					$timePublish_print = strtotime($this->datetimeFromDB($row['publish_print']));
					$timeSubmitted = strtotime($this->datetimeFromDB($row['date_submitted']));
					$completedPrintCount++;
					$totalElapsedTimePrint += ceil(($timePublish_online - $timeSubmitted) / 60 / 60 / 24);
				}

				if($row['publish_online'] && $row['publish_online'] != "0000-00-00 00:00:00") {
					$timePublish_online = strtotime($this->datetimeFromDB($row['publish_online']));
					$timeDecided = strtotime($this->datetimeFromDB($row['date_decided']));

					$decidedOnlineCount++;
					$val = ceil(($timePublish_online - $timeDecided) / 60 / 60 / 24);
					$totalElapsedTimeOnlineDecided += $val;
				}

				if($row['publish_print'] && $row['publish_print'] != "0000-00-00 00:00:00") {
					$timePublish_print = strtotime($this->datetimeFromDB($row['publish_print']));
					$timeDecided = strtotime($this->datetimeFromDB($row['date_decided']));
					$decidedPrintCount++;

					$val = ceil(($timePublish_print - $timeDecided) / 60 / 60 / 24);
					$totalElapsedTimePrintDecided += $val;
				}
			}

			$result->moveNext();
		}

		$result->Close();
		unset($result);

		// Calculate percentages where necessary
		if ($returner['numReviewedSubmissions'] != 0) {
			$returner['submissionsAcceptPercent'] = round($returner['submissionsAccept'] * 100 / $returner['numReviewedSubmissions']);
			$returner['submissionsDeclinePercent'] = round($returner['submissionsDecline'] * 100 / $returner['numReviewedSubmissions']);
			$returner['submissionsRevisePercent'] = round($returner['submissionsRevise'] * 100 / $returner['numReviewedSubmissions']);
			$returner['statUnassignedPercent'] = round($returner['statUnassigned'] * 100 / $returner['numSubmissions']);
			$returner['statPendingPercent'] = round($returner['statPending'] * 100 / $returner['numSubmissions']);
			$returner['numReviewedSubmissionsPercent'] = round($returner['numReviewedSubmissions'] * 100 / $returner['numSubmissions']);
		}

		if ($timeToPublicationCount != 0) {
			// Keep one sig fig
			$returner['daysToPublication'] = round($totalTimeToPublication / $timeToPublicationCount / 60 / 60 / 24);
		}

		if($completedOnlineCount) {
			$returner['statDaysToSubmissionOnline'] = round($totalElapsedTimeOnline / $completedOnlineCount);
		}
		if($completedPrintCount) {
			$returner['statDaysToSubmissionPrint'] = round($totalElapsedTimePrint / $completedPrintCount);
		}

		if($decidedOnlineCount) {
			$returner['statDaysFromAcceptanceOnline'] = round($totalElapsedTimeOnlineDecided / $decidedOnlineCount);
		}
		if($decidedPrintCount) {
			$returner['statDaysFromAcceptancePrint'] = round($totalElapsedTimePrintDecided / $decidedPrintCount);
		}
		return $returner;

	}

	/**
	 * Get statistics about users in the system.
	 * Returns a map of name => value pairs.
	 * @param $journalId int The journal to fetch statistics for
	 * @param $dateStart date optional
	 * @param $dateEnd date optional
	 * @return array
	 */
	function getUserStatistics($journalId, $dateStart = null, $dateEnd = null) {
		$roleDao =& DAORegistry::getDAO('RoleDAO');

		// Get count of total users for this journal
		$result =& $this->retrieve(
			'SELECT COUNT(DISTINCT r.user_id) FROM roles r, users u WHERE r.user_id = u.user_id AND r.journal_id = ?' .
			($dateStart !== null ? ' AND u.date_registered >= ' . $this->datetimeToDB($dateStart) : '') .
			($dateEnd !== null ? ' AND u.date_registered <= ' . $this->datetimeToDB($dateEnd) : ''),
			$journalId
		);

		$returner = array(
			'totalUsersCount' => $result->fields[0]
		);

		$result->Close();
		unset($result);

		// Get user counts for each role.
		$result =& $this->retrieve(
			'SELECT r.role_id, COUNT(r.user_id) AS role_count FROM roles r LEFT JOIN users u ON (r.user_id = u.user_id) WHERE r.journal_id = ?' .
			($dateStart !== null ? ' AND u.date_registered >= ' . $this->datetimeToDB($dateStart) : '') .
			($dateEnd !== null ? ' AND u.date_registered <= ' . $this->datetimeToDB($dateEnd) : '') .
			'GROUP BY r.role_id',
			$journalId
		);

		while (!$result->EOF) {
			$row = $result->GetRowAssoc(false);
			$returner[$roleDao->getRolePath($row['role_id'])] = $row['role_count'];
			$result->moveNext();
		}

		$result->Close();
		unset($result);

		return $returner;
	}

	/**
	 * Get statistics about subscriptions.
	 * @param $journalId int The journal to fetch statistics for
	 * @param $dateStart date optional
	 * @param $dateEnd date optional
	 * @return array
	 */
	function getSubscriptionStatistics($journalId, $dateStart = null, $dateEnd = null) {
		$result =& $this->retrieve(
			'SELECT	st.type_id,
				sts.setting_value AS type_name,
				count(s.subscription_id) AS type_count
			FROM	subscription_types st
				LEFT JOIN journals j ON (j.journal_id = st.journal_id)
				LEFT JOIN subscription_type_settings sts ON (st.type_id = sts.type_id AND sts.setting_name = ? AND sts.locale = j.primary_locale),
				subscriptions s
			WHERE	st.journal_id = ?
				AND s.type_id = st.type_id' .
			($dateStart !== null ? ' AND s.date_start >= ' . $this->datetimeToDB($dateStart) : '') .
			($dateEnd !== null ? ' AND s.date_start <= ' . $this->datetimeToDB($dateEnd) : '') .
			' GROUP BY st.type_id, sts.setting_value',
			array('name', $journalId)
		);

		$returner = array();

		while (!$result->EOF) {
			$row = $result->getRowAssoc(false);
			$returner[$row['type_id']] = array(
				'name' => $row['type_name'],
				'count' => $row['type_count']
			);
			$result->moveNext();
		}
		$result->Close();
		unset($result);

		return $returner;
	}

	/**
	 * Get statistics about issues in the system.
	 * Returns a map of name => value pairs.
	 * @param $journalId int The journal to fetch statistics for
	 * @param $dateStart date The publish date to search from; optional
	 * @param $dateEnd date The publish date to search to; optional
	 * @return array
	 */
	function getIssueStatistics($journalId, $dateStart = null, $dateEnd = null) {
		$result =& $this->retrieve(
			'SELECT COUNT(*) AS count, published FROM issues WHERE journal_id = ?' .
			($dateStart !== null ? ' AND date_published >= ' . $this->datetimeToDB($dateStart) : '') .
			($dateEnd !== null ? ' AND date_published <= ' . $this->datetimeToDB($dateEnd) : '') .
			' GROUP BY published',
			$journalId
		);

		$returner = array(
			'numPublishedIssues' => 0,
			'numUnpublishedIssues' => 0
		);

		while (!$result->EOF) {
			$row = $result->GetRowAssoc(false);

			if ($row['published']) {
				$returner['numPublishedIssues'] = $row['count'];
			} else {
				$returner['numUnpublishedIssues'] = $row['count'];
			}
			$result->moveNext();
		}

		$result->Close();
		unset($result);

		$returner['numIssues'] = $returner['numPublishedIssues'] + $returner['numUnpublishedIssues'];

		return $returner;
	}

	/**
	 * Get statistics about reviewers in the system.
	 * Returns a map of name => value pairs.
	 * @param $journalId int The journal to fetch statistics for
	 * @param $dateStart date The publish date to search from; optional
	 * @param $dateEnd date The publish date to search to; optional
	 * @return array
	 */
	function getReviewerStatistics($journalId, $sectionIds, $dateStart = null, $dateEnd = null) {
	
		import('classes.submission.common.Action');
	
		$params = array($journalId);
		if (!empty($sectionIds)) {
			$sectionSql = ' AND (a.section_id = ?';
			$params[] = array_shift($sectionIds);
			foreach ($sectionIds as $sectionId) {
				$sectionSql .= ' OR a.section_id = ?';
				$params[] = $sectionId;
			}
			$sectionSql .= ')';
		} else $sectionSql = '';

		$sql =	'SELECT	a.article_id,
				a.date_submitted,
				af.date_uploaded AS date_rv_uploaded,
				ed.date_decided as ed_date_decided,
				ed2.date_decided as ed_decision_rev_required
			FROM
				articles a
				LEFT JOIN article_files af ON af.article_id = a.article_id
					AND af.file_id = a.revised_file_id
					AND af.revision = 1
				LEFT JOIN edit_decisions ed ON (a.article_id = ed.article_id AND ed.decision NOT IN ('.SUBMISSION_EDITOR_DECISION_PENDING_REVISIONS.','.                    SUBMISSION_EDITOR_DECISION_RESUBMIT.'))
				LEFT JOIN edit_decisions ed2 ON (a.article_id = ed2.article_id AND ed2.decision = '.SUBMISSION_EDITOR_DECISION_PENDING_REVISIONS.')
			WHERE	a.journal_id = ?';

			if($dateType == "subDate") {
				$sql .= ($dateStart !== null ? ' AND a.date_submitted >= ' . $this->datetimeToDB($dateStart) : '') .
						($dateEnd !== null ? ' AND a.date_submitted <= ' . $this->datetimeToDB($dateEnd) : '');
			} else {
				$sql .= ($dateStart !== null ? ' AND ed.date_decided >= ' . $this->datetimeToDB($dateStart) : '') .
						($dateEnd !== null ? ' AND ed.date_decided <= ' . $this->datetimeToDB($dateEnd) : '');
			}

			$sql .= $sectionSql;

		$result =& $this->retrieve($sql, $params);

		$returner = array(
			'reviewsCount' => 0,
			'reviewerScore' => 0,
			'daysPerReview' => 0,
			'reviewerAddedCount' => 0,
			'reviewerCount' => 0,
			'reviewedSubmissionsCount' => 0,
			'daysPerReviewNoRevise' => 0
		);

		$scoredReviewsCount = 0;
		$totalScore = 0;
		$completedReviewsCount = 0;
		$totalElapsedTime = 0;
		$reviewerList = array();
		$articleIds = array();

		while (!$result->EOF) {
			$row = $result->GetRowAssoc(false);
			$returner['reviewsCount']++;
			if (!empty($row['quality'])) {
				$scoredReviewsCount++;
				$totalScore += $row['quality'];
			}

			$articleIds[] = $row['article_id'];

			if (!empty($row['reviewer_id']) && !in_array($row['reviewer_id'], $reviewerList)) {
				$returner['reviewerCount']++;
				$dateRegistered = strtotime($this->datetimeFromDB($row['date_registered']));
				if (($dateRegistered >= $dateStart || $dateStart === null) && ($dateRegistered <= $dateEnd || $dateEnd == null)) {
					$returner['reviewerAddedCount']++;
				}
				array_push($reviewerList, $row['reviewer_id']);
			}

			if (!empty($row['date_submitted']) && !empty($row['ed_date_decided'])) {
				$timeReviewVersionUploaded = strtotime(date("Y-m-d",strtotime($this->datetimeFromDB($row['ed_date_decided']))));
				$timeCompleted = strtotime(date("Y-m-d",strtotime($this->datetimeFromDB($row['date_submitted']))));

				$completedReviewsCount++;
				$totalTimeInReview = round(($timeReviewVersionUploaded - $timeCompleted) / 60 / 60 / 24);
				$totalElapsedTime += $totalTimeInReview;
//print_r($row);

				// calculate a second time
				// same as above, but removing the time the revision is spent with the author ( remove the time the editors decision is between "revisions required" or "re submit for review" and the most recent author upload)
				if($row['date_rv_uploaded']) {
					$timeInReview = strtotime(date("Y-m-d",strtotime($this->datetimeFromDB($row['date_rv_uploaded']))));
					$ed_decision_rev_required = strtotime(date("Y-m-d",strtotime($this->datetimeFromDB($row['ed_decision_rev_required']))));
					if ($timeInReview) {
						$completedReviewsCountInReview++;
						$timeInReviewDays = round(($timeInReview - $ed_decision_rev_required) / 60 / 60 / 24);
						if($timeInReviewDays < 0) $timeInReviewDays = 0;
						$totalElapsedTimeInReview += $totalTimeInReview - $timeInReviewDays;
					}
				} else {
						$completedReviewsCountInReview++;
				}
				// echo $row["article_id"].") "
				// 	.$this->datetimeFromDB($row['ed_date_decided'])." - "
				// 	.$this->datetimeFromDB($row['date_submitted'])." = $totalTimeInReview - "
				// 	.$this->datetimeFromDB($row['date_rv_uploaded'])." = ($totalTimeInReview - $timeInReviewDays) = ".($totalTimeInReview - $timeInReviewDays)
				// 	."<br/>";
			}
			$totalTimeInReview = 0;
			$timeInReviewDays = 0;
			$result->moveNext();
		}

		$result->Close();
		unset($result);

		if ($scoredReviewsCount > 0) {
			// To one decimal place
			$returner['reviewerScore'] = round($totalScore * 10 / $scoredReviewsCount) / 10;
		}
		if ($completedReviewsCount > 0) {
			$seconds = $totalElapsedTime / $completedReviewsCount;
			$returner['daysPerReview'] = $seconds;
		}

		if ($completedReviewsCountInReview > 0) {
			$seconds = $totalElapsedTimeInReview / $completedReviewsCountInReview;
			$returner['daysPerReviewNoRevise'] = $seconds;
		}

		$articleIds = array_unique($articleIds);
		$returner['reviewedSubmissionsCount'] = count($articleIds);

		return $returner;
	}
}

?>
