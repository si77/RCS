<?php

/**
 * @file classes/tasks/ReviewReminder.inc.php
 *
 * Copyright (c) 2003-2011 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class ReviewReminder
 * @ingroup tasks
 *
 * @brief Class to perform automated reminders for reviewers.
 */

// $Id$

ob_start();

import('lib.pkp.classes.scheduledTask.ScheduledTask');
import('classes.article.log.ArticleEmailLogEntry');
import('classes.article.log.ArticleLog');

class Day3ReviewReminder extends ScheduledTask {

	/**
	 * Constructor.
	 */
	function ReviewReminder() {
		$this->ScheduledTask();
	}

	function sendReminder ($reviewAssignment, $article, $journal) {
		$reviewAssignmentDao =& DAORegistry::getDAO('ReviewAssignmentDAO');
		$userDao =& DAORegistry::getDAO('UserDAO');
		$reviewId = $reviewAssignment->getId();

		$reviewer =& $userDao->getUser($reviewAssignment->getReviewerId());
		if (!isset($reviewer)) return false;

		import('classes.mail.ArticleMailTemplate');

		$reviewerAccessKeysEnabled = $journal->getSetting('reviewerAccessKeysEnabled');

		// email templates are stored in the database. These email keys HAVE to be added for emails to work

		// REVIEW_REMIND_AUTO_ONECLICK_3D
		// REVIEW_REMIND_AUTO_3DAY

		$email = new ArticleMailTemplate($article, $reviewerAccessKeysEnabled?'REVIEW_REMIND_AUTO_ONECLICK_3D':'REVIEW_REMIND_AUTO_3DAY', null, false, $journal);
		$email->setJournal($journal);
		$email->setFrom($journal->getSetting('contactEmail'), $journal->getSetting('contactName'));
		$email->addRecipient("mwillis.fb@googlemail.com"/*$reviewer->getEmail()*/, $reviewer->getFullName());
		$email->setAssoc(ARTICLE_EMAIL_REVIEW_REMIND, ARTICLE_EMAIL_TYPE_REVIEW, $reviewId);

		$subject = $email->getSubject($journal->getPrimaryLocale());
//		echo "Article: ".$article->getId()."\n";
//		print_r($article);
		$email->setSubject($subject);

		$email->setBody($email->getBody($journal->getPrimaryLocale()));

		$urlParams = array();
		if ($reviewerAccessKeysEnabled) {
			import('lib.pkp.classes.security.AccessKeyManager');
			$accessKeyManager = new AccessKeyManager();

			// Key lifetime is the typical review period plus four weeks
			$keyLifetime = ($journal->getSetting('numWeeksPerReview') + 4) * 7;
			$urlParams['key'] = $accessKeyManager->createKey('ReviewerContext', $reviewer->getId(), $reviewId, $keyLifetime);
		}
		$submissionReviewUrl = Request::url($journal->getPath(), 'reviewer', 'submission', $reviewId, $urlParams);

		// Format the review due date
		$reviewDueDate = strtotime($reviewAssignment->getDateDue());
		$dateFormatShort = Config::getVar('general', 'date_format_short');
		if ($reviewDueDate == -1) $reviewDueDate = $dateFormatShort; // Default to something human-readable if no date specified
		else $reviewDueDate = strftime($dateFormatShort, $reviewDueDate);

		$paramArray = array(
			'reviewerName' => $reviewer->getFullName(),
			'reviewerUsername' => $reviewer->getUsername(),
			'journalUrl' => $journal->getUrl(),
			'reviewerPassword' => $reviewer->getPassword(),
			'reviewDueDate' => $reviewDueDate,
			'weekLaterDate' => strftime(Config::getVar('general', 'date_format_short'), strtotime('+1 week')),
			'editorialContactSignature' => $journal->getSetting('contactName') . "\n" . $journal->getLocalizedTitle(),
			'passwordResetUrl' => Request::url($journal->getPath(), 'login', 'resetPassword', $reviewer->getUsername(), array('confirm' => Validation::generatePasswordResetHash($reviewer->getId()))),
			'submissionReviewUrl' => $submissionReviewUrl
		);
		$email->assignParams($paramArray);

//		echo "Article Email off\n";

		$entry = new ArticleEmailLogEntry();

		// Log data
		$entry->setEventType($email->eventType);
		$entry->setAssocType($email->assocType);
		$entry->setAssocId($email->assocId);

		// Email data
		$entry->setSubject($email->getSubject());
		$entry->setBody($email->getBody());
		$entry->setFrom($email->getFromString(false));
		$entry->setRecipients($email->getRecipientString());
		$entry->setCcs($email->getCcString());
		$entry->setBccs($email->getBccString());

		// Add log entry
		$logEntryId = ArticleLog::logEmailEntry($article->getId(), $entry);

		echo "sending: ".$email->getRecipientString()."\n:";
		$email->send();

//		$reviewAssignment->setDateReminded(Core::getCurrentDate());
//		$reviewAssignment->setReminderWasAutomatic(1);
//		$reviewAssignmentDao->updateReviewAssignment($reviewAssignment);

	}

	function execute() {
 		$article = null;
		$journal = null;

		$reviewAssignmentDao =& DAORegistry::getDAO('ReviewAssignmentDAO');
		$articleDao =& DAORegistry::getDAO('ArticleDAO');
		$journalDao =& DAORegistry::getDAO('JournalDAO');

		$day3Assignments =& $reviewAssignmentDao->getIncompleteReviewAssignments();


		foreach ($day3Assignments as $reviewAssignment) {
			// Fetch the Article and the Journal if necessary.
			if ($article == null || $article->getId() != $reviewAssignment->getSubmissionId()) {
				unset($article);
				$article =& $articleDao->getArticle($reviewAssignment->getSubmissionId());
				if(!$article) {
					//echo "New bug?\n";
					continue;
				}
				if ($journal == null || $journal->getId() != $article->getJournalId()) {
					unset($journal);
					$journal =& $journalDao->getJournal($article->getJournalId());

					$inviteReminderEnabled = $journal->getSetting('remindForInvite');
					$submitReminderEnabled = $journal->getSetting('remindForSubmit');
					$inviteReminderDays = $journal->getSetting('numDaysBeforeInviteReminder');
					$submitReminderDays = $journal->getSetting('numDaysBeforeSubmitReminder');
				}
			}

			if ($article->getStatus() != STATUS_QUEUED) continue;


			$shouldRemind = false;

			$dateDue = $reviewAssignment->getDateDue();
//			$dateDue = "2012-05-25";								// DEBUG LINE

			if($dateDue != null) {
				$dateDue = strtotime(date("Y-m-d",strtotime($dateDue))); // hack to get the seconds of JUST the day - not any time if chosen
				$dateIn3Days = strtotime(date("Y-m-d",time())) + (86400 * 3);

				if($dateDue != $dateIn3Days) {
					$shouldRemind = false;
				} else {
					$shouldRemind = true;
				}

			}

			if ($shouldRemind) $this->sendReminder ($reviewAssignment, $article, $journal);
		}
	}
}

?>
