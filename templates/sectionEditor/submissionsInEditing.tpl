{**
 * submissionsInEditing.tpl
 *
 * Copyright (c) 2003-2011 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Show section editor's submissions in editing.
 *
 * $Id$
 *}
<div id="submissions">
<table width="100%" class="listing">
	<tr><td colspan="9" class="headseparator">&nbsp;</td></tr>
	<tr class="heading" valign="bottom">
	<td width="5%">{sort_search key="common.id" sort="id"}</td>
		<td width="5%">{sort_search key="submissions.submit" sort="submitDate"}</td>
		<td width="5%">{translate key="submissions.acceptance"}</td>
		<td width="5%">{sort_search key="submissions.sec" sort="section"}</td>
		<td width="20%">{sort_search key="article.authors" sort="authors"}</td>
		<td width="25%">{sort_search key="article.title" sort="title"}</td>
		<td width="10%">{sort_search key="submission.copyeditRequest" sort="subCopyedit"}</td>
		<td width="10%">{sort_search key="submission.copyeditUnderway" sort="subCopyedit"}</td>
		<td width="10%">{sort_search key="submission.copyeditComplete" sort="subCopyedit"}</td>
	</tr>
	<tr><td colspan="9" class="headseparator">&nbsp;</td></tr>

{iterate from=submissions item=submission}

	{assign var="layoutEditorProofSignoff" value=$submission->getSignoff('SIGNOFF_PROOFREADING_LAYOUT')}
	{assign var="layoutSignoff" value=$submission->getSignoff('SIGNOFF_LAYOUT')}
	{assign var="copyeditorFinalSignoff" value=$submission->getSignoff('SIGNOFF_COPYEDITING_FINAL')}
	{assign var="articleId" value=$submission->getArticleId()}
	{assign var="highlightClass" value=$submission->getHighlightClass()}
	{assign var="initialCopyeditSignoff" value=$submission->getSignoff('SIGNOFF_COPYEDITING_INITIAL')}
	<tr valign="top"{if $highlightClass} class="{$highlightClass|escape}"{/if}>
		<td>{$submission->getArticleId()}</td>
		<td>{$submission->getDateSubmitted()|date_format:$dateFormatTrunc}</td>
		<td>{$submission->getLatestAcceptanceDate()|date_format:$dateFormatShort}</td>
		<td>{$submission->getSectionAbbrev()|escape}</td>
		<td>{$submission->getAuthorString(true)|truncate:40:"..."|escape}</td>
		<td><a href="{url op="submissionEditing" path=$articleId}" class="action">{$submission->getLocalizedTitle()|strip_unsafe_html|truncate:60:"..."}</a></td>
		<td>{$initialCopyeditSignoff->getDateUnderway()|date_format:$dateFormatShort|default:"&mdash;"}</td>
		<td>{$initialCopyeditSignoff->getDateCompleted()|date_format:$dateFormatShort|default:"&mdash;"}</td>
		<td>{$initialCopyeditSignoff->getDateCompleted()|date_format:$dateFormatShort|default:"&mdash;"}</td>
	</tr>
	<tr>
		<td colspan="9" class="{if $submissions->eof()}end{/if}separator">&nbsp;</td>
	</tr>
{/iterate}
{if $submissions->wasEmpty()}
	<tr>
		<td colspan="9" class="nodata">{translate key="submissions.noSubmissions"}</td>
	</tr>
	<tr>
		<td colspan="9" class="endseparator">&nbsp;</td>
	<tr>
{else}
	<tr>
		<td colspan="4" align="left">{page_info iterator=$submissions}</td>
		<td colspan="5" align="right">{page_links anchor="submissions" name="submissions" iterator=$submissions searchField=$searchField searchMatch=$searchMatch search=$search dateFromDay=$dateFromDay dateFromYear=$dateFromYear dateFromMonth=$dateFromMonth dateToDay=$dateToDay dateToYear=$dateToYear dateToMonth=$dateToMonth dateSearchField=$dateSearchField section=$section sort=$sort sortDirection=$sortDirection}</td>
	</tr>
{/if}
</table>
</div>

