{**
 * sectionEditorRecomendation.tpl
 *
 * Copyright (c) 2003-2011 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Subtemplate defining the editor decision table.
 *
 * $Id$
 *}
<div id="sectionEditorRecomendation">
       <h3>{translate key="submission.sectionEditorRecommendation"}</h3>
       {if $current_role == "sectionEditor"}
       {if $sectionEditorRecommendationComment}
	       <p><strong>Editor Has Been Notified</strong></p>
       {/if}
	       <form method="post" action="{url op="recordSectionEditorRecommendation"}">
		       <table id="table1" width="100%" class="data">
		       <tr valign="top">
			       <td class="label" width="20%">Select Recommendation</td>
			       <td width="80%" class="value">
					       <input type="hidden" name="articleId" value="{$submission->getId()}" />
					       <select name="decision" size="1" class="selectMenu">
						       <option label="Choose One" value="">Choose One</option> 
							<option label="Very Good" value="5" selected="selected">Very Good</option> 
							<option label="Good" value="4">Good</option> 
							<option label="Average" value="3">Average</option> 
							<option label="Poor" value="2">Poor</option> 
							<option label="Very Poor" value="1">Very Poor</option>
					       </select>
			       </td>
		       </tr>
		       <tr valign="top">
			       <td class="label" width="20%">Comments</td>
			       <td width="80%" class="value">
					       <textarea name="comment" size="1" maxlength="500" class="sectionEditorComment">{$sectionEditorRecommendationComment}</textarea>
					       
					       <input type="submit" onclick="return confirm('{translate|escape:"jsparam" key="editor.sectionEditorRecommendation.confirmDecision"}')" name="submit" value="Notify Editor"  class="button" />
			       </td>
		       </tr>
		       </table>
	       </form>
       {else}
		       <table id="table1" width="100%" class="data">
		       <tr valign="top">
			       <td class="label" width="20%">Recommendation</td>
			       <td width="80%" class="value">
					       {translate key="$sectionEditorRecommendationRatingText"}
			       </td>
		       </tr>
		       <tr valign="top">
			       <td class="label" width="20%">Comments</td>
			       <td width="80%" class="value">
					       {$sectionEditorRecommendationComment|nl2br}
			       </td>
		       </tr>
		       </table>
	       </form>
       {/if}
</div>
 