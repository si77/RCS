{**
 * publishing.tpl
 *
 * Copyright (c) 2003-2011 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Subtemplate defining the publishing table.
 *
 * $Id$
 *}

<div id="publishing">
	<h3>{translate key="submission.publishing"}</h3>

	<form action="{url op="scheduleForPublishing" path=$submission->getId()}" method="post">
		<p>
			Online:
			{html_select_date start_year="2010" end_year="+4" field_array="online" field_order="DMY" time=$onlineDate|default:"0000-00-00"}

			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			Print:
			<select disabled="disabled">
				<option>01</option>
			</select>
			{html_select_date start_year="2010" end_year="+4" field_array="print" field_order="MY" display_days="false" time=$printDate|default:"0000-00-00"}

			<input type="submit" value="Save" class="button" />
		</p>

	</form>
</div>
