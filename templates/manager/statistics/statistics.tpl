{**
 * statistics.tpl
 *
 * Copyright (c) 2003-2011 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Subtemplate defining the statistics table.
 *
 * $Id$
 *}
{* WARNING: This page should be kept roughly synchronized with the
   implementation of reader statistics in the About page.          *}
<div id="statistics">
<h3>{translate key="manager.statistics.statistics"}</h3>
<p>{translate key="manager.statistics.statistics.description"}</p>
<div id="selectSections">
<p>{translate key="manager.statistics.statistics.selectSections"}</p>
<form action="{url op="saveStatisticsSections"}" method="post">
	<select name="sectionIds[]" class="selectMenu" multiple="multiple" size="5">
		<option value="-1">All Sections</option>
		{foreach from=$sections item=section}
			<option {if in_array($section->getId(), $sectionIds)}selected="selected" {/if}value="{$section->getId()}">{$section->getLocalizedTitle()}</option>
		{/foreach}
	</select><br/>&nbsp;<br/>

	{foreach from=$sections item=section}
		<input name="allSectionIds[]" type="hidden" value="{$section->getId()}" />
	{/foreach}

	<input type="submit" value="{translate key="common.record"}" class="button defaultButton"/>
</form>
</div>
<br/>

<form action="{url op="savePublicStatisticsList"}" method="post">

	Start Date:
	<input name="startDate" value="{$startDate|default:"0000-00-00"|date_format:"%Y-%m-%d"}" type="text" /> YYYY-MM-DD
	<br /><br />

	End Date:
	<input name="endDate" value="{$endDate|default:"0000-00-00"|date_format:"%Y-%m-%d"}" type="text" /> YYYY-MM-DD
	<br />
	Date Type:
	<select name="dateType">
		<option value="subDate" {if $dateType eq 'subDate'}selected="selected"{/if}>Submission Date</option>
		<option value="expDate" {if $dateType neq 'subDate'}selected="selected"{/if}>Editors Accept Date</option>
	</select>

<table width="100%" class="data">
	<tr valign="top">
		<td width="25%" class="label"><h4>{translate key="common.year"}</h4></td>
		<td width="75%" colspan="2" class="value">
			<h4><a class="action" href="{url statisticsYear=$statisticsYear-1}">{translate key="navigation.previousPage"}</a>&nbsp;{$statisticsYear|escape}&nbsp;<a class="action" href="{url statisticsYear=$statisticsYear+1}">{translate key="navigation.nextPage"}</a></h4>
		</td>
	</tr>

<!--	<tr valign="top">
		<td class="label"><input type="checkbox" id="statNumPublishedIssues" name="statNumPublishedIssues" {if $statNumPublishedIssues}checked="checked" {/if}/><label for="statNumPublishedIssues">{translate key="manager.statistics.statistics.numIssues"}</label></td>
		<td colspan="2" class="value">{$issueStatistics.numPublishedIssues}</td>
	</tr>

	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statItemsPublished" name="statItemsPublished" {if $statItemsPublished}checked="checked" {/if}/><label for="statItemsPublished">{translate key="manager.statistics.statistics.itemsPublished"}</label></td>
		<td width="60%" colspan="2" class="value">{$articleStatistics.numPublishedSubmissions}</td>
	</tr>
	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statNumSubmissions" name="statNumSubmissions" {if $statNumSubmissions}checked="checked" {/if}/><label for="statNumSubmissions">{translate key="manager.statistics.statistics.numSubmissions"}</label></td>
		<td width="60%" colspan="2" class="value">{$articleStatistics.numSubmissions}</td>
	</tr>
	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statPeerReviewed" name="statPeerReviewed" {if $statPeerReviewed}checked="checked" {/if}/><label for="statPeerReviewed">{translate key="manager.statistics.statistics.peerReviewed"}</label></td>
		<td width="60%" colspan="2" class="value">{$limitedArticleStatistics.numReviewedSubmissions}</td>
	</tr>
	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statCountAccept" name="statCountAccept" {if $statCountAccept}checked="checked" {/if}/>&nbsp;&nbsp;<label for="statCountAccept">{translate key="manager.statistics.statistics.count.accept"}</label></td>
		<td width="60%" colspan="2" class="value">{translate key="manager.statistics.statistics.count.value" count=$limitedArticleStatistics.submissionsAccept percentage=$limitedArticleStatistics.submissionsAcceptPercent}</td>
	</tr>
	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statCountDecline" name="statCountDecline" {if $statCountDecline}checked="checked" {/if}/>&nbsp;&nbsp;<label for="statCountDecline">{translate key="manager.statistics.statistics.count.decline"}</label></td>
		<td width="60%" colspan="2" class="value">{translate key="manager.statistics.statistics.count.value" count=$limitedArticleStatistics.submissionsDecline percentage=$limitedArticleStatistics.submissionsDeclinePercent}</td>
	</tr>
	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statCountRevise" name="statCountRevise" {if $statCountRevise}checked="checked" {/if}/>&nbsp;&nbsp;<label for="statCountRevise">{translate key="manager.statistics.statistics.count.revise"}</label></td>
		<td width="60%" colspan="2" class="value">{translate key="manager.statistics.statistics.count.value" count=$limitedArticleStatistics.submissionsRevise percentage=$limitedArticleStatistics.submissionsRevisePercent}</td>
	</tr>
	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statDaysPerReview" name="statDaysPerReview" {if $statDaysPerReview}checked="checked" {/if}/>&nbsp;&nbsp;<label for="statDaysPerReview">{translate key="manager.statistics.statistics.daysPerReview"}</label></td>
		<td colspan="2" class="value">
			{assign var=daysPerReview value=$reviewerStatistics.daysPerReview}
			{math equation="round($daysPerReview)"}
		</td>
	</tr>
	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statDaysToPublication" name="statDaysToPublication" {if $statDaysToPublication}checked="checked" {/if}/>&nbsp;&nbsp;<label for="statDaysToPublication">{translate key="manager.statistics.statistics.daysToPublication"}</label></td>
		<td colspan="2" class="value">{$limitedArticleStatistics.daysToPublication}</td>
	</tr>
	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statRegisteredUsers" name="statRegisteredUsers" {if $statRegisteredUsers}checked="checked" {/if}/><label for="statRegisteredUsers">{translate key="manager.statistics.statistics.registeredUsers"}</label></td>
		<td colspan="2" class="value">{translate key="manager.statistics.statistics.totalNewValue" numTotal=$allUserStatistics.totalUsersCount numNew=$userStatistics.totalUsersCount}</td>
	</tr>
	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statRegisteredReaders" name="statRegisteredReaders" {if $statRegisteredReaders}checked="checked" {/if}/><label for="statRegisteredReaders">{translate key="manager.statistics.statistics.registeredReaders"}</label></td>
		<td colspan="2" class="value">{translate key="manager.statistics.statistics.totalNewValue" numTotal=$allUserStatistics.reader|default:"0" numNew=$userStatistics.reader|default:"0"}</td>
	</tr>

	{if $currentJournal->getSetting('publishingMode') == $smarty.const.PUBLISHING_MODE_SUBSCRIPTION}
		<tr valign="top">
			<td colspan="3" class="label"><input type="checkbox" id="statSubscriptions" name="statSubscriptions" {if $statSubscriptions}checked="checked" {/if}/><label for="statSubscriptions">{translate key="manager.statistics.statistics.subscriptions"}</label></td>
		</tr>
		{foreach from=$allSubscriptionStatistics key=type_id item=stats}
		<tr valign="top">
			<td width="40%" class="label">&nbsp;&nbsp;{$stats.name}:</td>
			<td colspan="2" class="value">{translate key="manager.statistics.statistics.totalNewValue" numTotal=$stats.count|default:"0" numNew=$subscriptionStatistics.$type_id.count|default:"0"}</td>
		</tr>
		{/foreach}
	{/if}

	<tr valign="top">
		<td width="40%" colspan="3" class="label"><input type="checkbox" id="statViews" name="statViews" {if $statViews}checked="checked" {/if}/><label for="statViews">{translate key="manager.statistics.statistics.articleViews"}</label></td>
	</tr>
-->

	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statNumSubmissions" name="statNumSubmissions" {if $statNumSubmissions}checked="checked" {/if}/><label for="statNumSubmissions">{translate key="manager.statistics.statistics.numSubmissions"}</label></td>
		<td width="60%" colspan="2" class="value">{$articleStatistics.numSubmissions}</td>
	</tr>

	<tr><td>&nbsp;</td></tr>

	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statItemsInCat" name="statItemsInCat" {if $statItemsInCat}checked="checked" {/if}/><label for="statItemsInCat">{translate key="manager.statistics.statistics.statItemsInCat"}</label></td>
		<td width="60%" colspan="2" class="value">{$limitedArticleStatistics.numSubmissions} ({$totalInCatPercent}%)</td>
	</tr>

	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statUnassigned" name="statUnassigned" {if $statUnassigned}checked="checked" {/if}/><label for="statUnassigned">{translate key="manager.statistics.statistics.statUnassigned"}</label></td>
		<td width="60%" colspan="2" class="value">{$limitedArticleStatistics.statUnassigned} ({$limitedArticleStatistics.statUnassignedPercent}%)</td>
	</tr>

	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statPending" name="statPending" {if $statPending}checked="checked" {/if}/><label for="statPending">{translate key="manager.statistics.statistics.statPending"}</label></td>
		<td width="60%" colspan="2" class="value">{$limitedArticleStatistics.statPending} ({$limitedArticleStatistics.statPendingPercent}%)</td>
	</tr>

	<tr><td>&nbsp;</td></tr>


	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statPeerReviewed" name="statPeerReviewed" {if $statPeerReviewed}checked="checked" {/if}/><label for="statPeerReviewed">{translate key="manager.statistics.statistics.peerReviewed"}</label></td>
		<td width="60%" colspan="2" class="value">{$limitedArticleStatistics.numReviewedSubmissions} ({$limitedArticleStatistics.numReviewedSubmissionsPercent}%)</td>
	</tr>
	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statCountAccept" name="statCountAccept" {if $statCountAccept}checked="checked" {/if}/>&nbsp;&nbsp;<label for="statCountAccept">{translate key="manager.statistics.statistics.count.accept"}</label></td>
		<td width="60%" colspan="2" class="value">{translate key="manager.statistics.statistics.count.value" count=$limitedArticleStatistics.submissionsAccept percentage=$limitedArticleStatistics.submissionsAcceptPercent}</td>
	</tr>
	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statCountDecline" name="statCountDecline" {if $statCountDecline}checked="checked" {/if}/>&nbsp;&nbsp;<label for="statCountDecline">{translate key="manager.statistics.statistics.count.decline"}</label></td>
		<td width="60%" colspan="2" class="value">{translate key="manager.statistics.statistics.count.value" count=$limitedArticleStatistics.submissionsDecline percentage=$limitedArticleStatistics.submissionsDeclinePercent}</td>
	</tr>
	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statCountRevise" name="statCountRevise" {if $statCountRevise}checked="checked" {/if}/>&nbsp;&nbsp;<label for="statCountRevise">{translate key="manager.statistics.statistics.count.revise"}</label></td>
		<td width="60%" colspan="2" class="value">{translate key="manager.statistics.statistics.count.value" count=$limitedArticleStatistics.submissionsRevise percentage=$limitedArticleStatistics.submissionsRevisePercent}</td>
	</tr>
	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statDaysPerReview" name="statDaysPerReview" {if $statDaysPerReview}checked="checked" {/if}/>&nbsp;&nbsp;<label for="statDaysPerReview">{translate key="manager.statistics.statistics.daysPerReview"}</label></td>
		<td colspan="2" class="value">
			{assign var=daysPerReview value=$reviewerStatistics.daysPerReview}
			{math equation="round($daysPerReview)"}
		</td>
	</tr>
	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statDaysPerReview" name="statDaysPerReview" {if $statDaysPerReview}checked="checked" {/if}/>&nbsp;&nbsp;<label for="statDaysPerReview">{translate key="manager.statistics.statistics.statDaysPerReviewNoRevise"}</label></td>
		<td colspan="2" class="value">
			{assign var=statDaysPerReviewNoRevise value=$reviewerStatistics.daysPerReviewNoRevise}
			{math equation="round($statDaysPerReviewNoRevise)"}
		</td>
	</tr>

	<tr><td>&nbsp;</td></tr>


	<!-- Spacer, days to publish online -->
	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statDaysToSubmissionHeading" name="statDaysToSubmissionHeading" {if $statDaysToSubmissionHeading}checked="checked" {/if}/><label for="statDaysToSubmissionHeading">{translate key="manager.statistics.statistics.statDaysToSubmissionHeading"}</label></td>
		<td colspan="2" class="value">{$limitedArticleStatistics.statDaysToSubmissionHeading}</td>
	</tr>

	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statDaysToSubmissionOnline" name="statDaysToSubmissionOnline" {if $statDaysToSubmissionOnline}checked="checked" {/if}/>&nbsp;&nbsp;<label for="statDaysToSubmissionOnline">{translate key="manager.statistics.statistics.statDaysToSubmissionOnline"}</label></td>
		<td colspan="2" class="value">{$limitedArticleStatistics.statDaysToSubmissionOnline}</td>
	</tr>

	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statDaysToSubmissionPrint" name="statDaysToSubmissionPrint" {if $statDaysToSubmissionPrint}checked="checked" {/if}/>&nbsp;&nbsp;<label for="statDaysToSubmissionPrint">{translate key="manager.statistics.statistics.statDaysToSubmissionPrint"}</label></td>
		<td colspan="2" class="value">{$limitedArticleStatistics.statDaysToSubmissionPrint}</td>
	</tr>

	<tr><td>&nbsp;</td></tr>


	<!-- Spacer, days to publish online -->
	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statDaysFromAcceptanceHeading" name="statDaysFromAcceptanceHeading" {if $statDaysFromAcceptanceHeading}checked="checked" {/if}/><label for="statDaysFromAcceptanceHeading">{translate key="manager.statistics.statistics.statDaysFromAcceptanceHeading"}</label></td>
		<td colspan="2" class="value">{$limitedArticleStatistics.statDaysFromAcceptanceHeading}</td>
	</tr>

	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statDaysFromAcceptanceOnline" name="statDaysFromAcceptanceOnline" {if $statDaysFromAcceptanceOnline}checked="checked" {/if}/>&nbsp;&nbsp;<label for="statDaysFromAcceptanceOnline">{translate key="manager.statistics.statistics.statDaysFromAcceptanceOnline"}</label></td>
		<td colspan="2" class="value">{$limitedArticleStatistics.statDaysFromAcceptanceOnline}</td>
	</tr>

	<tr valign="top">
		<td width="40%" class="label"><input type="checkbox" id="statDaysFromAcceptancePrint" name="statDaysFromAcceptancePrint" {if $statDaysFromAcceptancePrint}checked="checked" {/if}/>&nbsp;&nbsp;<label for="statDaysFromAcceptancePrint">{translate key="manager.statistics.statistics.statDaysFromAcceptancePrint"}</label></td>
		<td colspan="2" class="value">{$limitedArticleStatistics.statDaysFromAcceptancePrint}</td>
	</tr>

</table>
<p>{translate key="manager.statistics.statistics.note"}</p>

{translate key="manager.statistics.statistics.makePublic"}<br/>
<input type="submit" class="button defaultButton" value="{translate key="common.record"}"/>
&nbsp;&nbsp;&nbsp;&nbsp;<a href="?{$csvArgs}" target="_blank" class="button">Download CSV</a>

</form>
</div>

