<?php
 
 /**
  * @file editorSummaryBlockPlugin.inc.php
  *
  * Copyright (c) 2003-2011 John Willinsky
  * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
  *
  * @class editorSummaryBlockPlugin
  * @ingroup plugins_blocks_editorSummary
  *
  * @brief Class for "editorSummary" block plugin
  */
 
 // $Id$
 
 
 import('lib.pkp.classes.plugins.BlockPlugin');
 
 class editorSummaryBlockPlugin extends BlockPlugin {
 	/**
 	 * Determine whether the plugin is enabled. Overrides parent so that
 	 * the plugin will be displayed during install.
 	 */
 	function getEnabled() {
 		if (!Config::getVar('general', 'installed')) return true;
 		return parent::getEnabled();
 	}
 
 	/**
 	 * Install default settings on system install.
 	 * @return string
 	 */
 	function getInstallSitePluginSettingsFile() {
 		return $this->getPluginPath() . '/settings.xml';
 	}
 
 	/**
 	 * Install default settings on journal creation.
 	 * @return string
 	 */
 	function getContextSpecificPluginSettingsFile() {
 		return $this->getPluginPath() . '/settings.xml';
 	}
 
 	/**
 	 * Get the block context. Overrides parent so that the plugin will be
 	 * displayed during install.
 	 * @return int
 	 */
 	function getBlockContext() {
 		if (!Config::getVar('general', 'installed')) return BLOCK_CONTEXT_RIGHT_SIDEBAR;
 		return parent::getBlockContext();
 	}
 
 	/**
 	 * Determine the plugin sequence. Overrides parent so that
 	 * the plugin will be displayed during install.
 	 */
 	function getSeq() {
 		if (!Config::getVar('general', 'installed')) return 1;
 		return parent::getSeq();
 	}
 
 	/**
 	 * Get the display name of this plugin.
 	 * @return String
 	 */
 	function getDisplayName() {
 		return Locale::translate('plugins.block.editorSummary.displayName');
 	}
 
 	/**
 	 * Get a description of the plugin.
 	 */
 	function getDescription() {
 		return Locale::translate('plugins.block.editorSummary.description');
 	}
 	/**
 	 * Override the block contents based on the current role being
 	 * browsed.
 	 * @return string
 	 */
 	function getBlockTemplateFilename() {
 		$journal =& Request::getJournal();
 		$user =& Request::getUser();
 		if (!$journal || !$user) return null;
 
 		$userId = $user->getId();
 		$journalId = $journal->getId();
 
 		$templateMgr =& TemplateManager::getManager();
 
 		switch (Request::getRequestedPage()) {
 			case 'editor':
 				$templateMgr->assign('role',"editor");
				$sectionUser = false;
				$sectionEditorSubmissionDAO =& DAORegistry::getDAO('EditorSubmissionDAO');
 			break;
 			case 'sectionEditor':
 				$templateMgr->assign('role',"sectionEditor");
				$sectionUser = $userId;
				$sectionEditorSubmissionDAO =& DAORegistry::getDAO('SectionEditorSubmissionDAO');
 			break;
 			default:
 				return null;
 			break;
 		}
  		/**
  		 * We are restricting this side block to JUST the user home page - causes major performance issues otherwise
  		 */
 		$noCalc = false;
  		if(Request::getRequestedOp() != "index" || count(Request::getRequestedArgs())) {
 			//$noCalc = true;
  		}
 
 		if($noCalc) {
 			
 			$summaryCount["reUploadedCount"] = 0; 
 			$summaryCount["reviewsReturnedCount"] = 0; 
 			$summaryCount["noReviewersCount"] = 0; 
 			$summaryCount["editorNotifiedCount"] = 0;
 			/*
 			$summaryCount["reUploadedCount"] = NULL; 
 			$summaryCount["reviewsReturnedCount"] = NULL; 
 			$summaryCount["noReviewersCount"] = NULL; 
 			$summaryCount["editorNotifiedCount"] = NULL;
 			*/ 
 			
 		} else {
  			
 			$summaryCount = $sectionEditorSubmissionDAO->getSubmissionInReviewIdBreakdown($sectionUser,$journal->getId());
 			
			if(isset($summaryCount["reUploaded"])) {
				$summaryCount["reUploadedCount"] = '('.count(array_unique($summaryCount["reUploaded"])).')'; 
				//$summaryCount["reUploadedCount"] = (count(array_unique($summaryCount["reUploaded"])) > 0) ? '<strong>&laquo;</strong>' : NULL;
			} else {
 				$summaryCount["reUploadedCount"] = 0; 
 				//$summaryCount["reUploadedCount"] = NULL; 
			}

			if(isset($summaryCount["reviewsReturned"])) {
	 			$summaryCount["reviewsReturnedCount"] = '('.count(array_unique($summaryCount["reviewsReturned"])).')'; 
	 			//$summaryCount["reviewsReturnedCount"] = (count(array_unique($summaryCount["reviewsReturned"])) > 0) ? '<strong>&laquo;</strong>' : NULL;
			} else {
 				$summaryCount["noReviewersCount"] = 0; 
				//$summaryCount["noReviewersCount"] = NULL; 
			}

			if(isset($summaryCount["noReviewers"])) {
	 			$summaryCount["noReviewersCount"] = '('.count(array_unique($summaryCount["noReviewers"])).')'; 
	 			//$summaryCount["noReviewersCount"] = (count(array_unique($summaryCount["noReviewers"])) > 0) ? '<strong>&laquo;</strong>' : NULL;
			} else {
 				$summaryCount["noReviewersCount"] = 0; 
 				//$summaryCount["noReviewersCount"] = NULL; 
			}

			if(isset($summaryCount["editorNotified"])) {
				$summaryCount["editorNotifiedCount"] = '('.count(array_unique($summaryCount["editorNotified"])).')'; 
				//$summaryCount["editorNotifiedCount"] = (count(array_unique($summaryCount["editorNotified"])) > 0) ? '<strong>&laquo;</strong>' : NULL;
			} else {
 				$summaryCount["editorNotifiedCount"] = 0;
 				//$summaryCount["editorNotifiedCount"] = NULL;  
			}
 		}		

  		$templateMgr->assign('summaryCount', $summaryCount);
  				
  		return 'summary.tpl';
 	}
 }
 
?>
