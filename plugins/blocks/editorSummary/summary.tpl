{**
 * copyeditor.tpl
 *
 * Copyright (c) 2003-2011 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Copyeditor navigation sidebar.
 *
 * $Id$
 *}
<div class="block" id="sidebarSummary">
       <span class="blockTitle">Summary</span>
       <span class="blockSubtitle">Article Summary</span>
       <ul>
  		{if $role == "editor"}
 			<li><a href="{url op="submissions" path="submissionsInReview"}/Uploaded">Revisions Uploaded</a>&nbsp;{if $summaryCount.reUploadedCount}{$summaryCount.reUploadedCount}{else}{/if}</li>
 			<li><a href="{url op="submissions" path="submissionsInReview"}/Returned">Reviews Returned</a>&nbsp;{if $summaryCount.reviewsReturnedCount}{$summaryCount.reviewsReturnedCount}{else}{/if}</li>
 			<li><a href="{url op="submissions" path="submissionsInReview"}/Allocated">No Reviews Allocated</a>&nbsp;{if $summaryCount.noReviewersCount}{$summaryCount.noReviewersCount}{else}{/if}</li>
 			<li><a href="{url op="submissions" path="submissionsInReview"}/Recommendation">Section Editor Recommendation</a>&nbsp;{if $summaryCount.editorNotifiedCount}{$summaryCount.editorNotifiedCount}{else}{/if}</li>
 		{else}
 			<li><a href="{url op="index" path="submissionsInReview"}/Uploaded">Revisions Uploaded</a>&nbsp;{if $summaryCount.reUploadedCount}{$summaryCount.reUploadedCount}{else}{/if}</li>
 			<li><a href="{url op="index" path="submissionsInReview"}/Returned">Reviews Returned</a>&nbsp;{if $summaryCount.reviewsReturnedCount}{$summaryCount.reviewsReturnedCount}{else}{/if}</li>
 			<li><a href="{url op="index" path="submissionsInReview"}/Allocated">No Reviews Allocated</a>&nbsp;{if $summaryCount.noReviewersCount}{$summaryCount.noReviewersCount}{else}{/if}</li>
  		{/if}
  	</ul>
</div>