<?php
 
 /**
  * @defgroup plugins_blocks_editorSummary
  */
  
 /**
  * @file plugins/blocks/editorSummary/index.php
  *
  * Copyright (c) 2003-2011 John Willinsky
  * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
  *
  * @ingroup plugins_blocks_editorSummary
  * @brief Wrapper for "editorSummary" block plugin.
  *
  */
 
 // $Id$
 
 
 require_once('editorSummaryBlockPlugin.inc.php');
 
 return new editorSummaryBlockPlugin();
 
?> 