<?php

/**
 * @file ReviewReportPlugin.inc.php
 *
 * Copyright (c) 2003-2011 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 * 
 * @class ReviewReportPlugin
 * @ingroup plugins_reports_review
 * @see ReviewReportDAO
 *
 * @brief Review report plugin
 */

//$Id$

import('classes.plugins.ReportPlugin');

class ReviewReportPlugin extends ReportPlugin {
	/**
	 * Called as a plugin is registered to the registry
	 * @param $category String Name of category plugin was registered to
	 * @return boolean True if plugin initialized successfully; if false,
	 * 	the plugin will not be registered.
	 */
	function register($category, $path) {
		$success = parent::register($category, $path);
		if ($success) {
			$this->import('ReviewReportDAO');
			$reviewReportDAO = new ReviewReportDAO();
			DAORegistry::registerDAO('ReviewReportDAO', $reviewReportDAO);
		}
		$this->addLocaleData();
		return $success;
	}

	/**
	 * Get the name of this plugin. The name must be unique within
	 * its category.
	 * @return String name of plugin
	 */
	function getName() {
		return 'ReviewReportPlugin';
	}

	function getDisplayName() {
		return Locale::translate('plugins.reports.reviews.displayName');
	}

	function getDescription() {
		return Locale::translate('plugins.reports.reviews.description');
	}

	function display(&$args) {
		$journal =& Request::getJournal();
		
		header('content-type: text/comma-separated-values');
		
		Locale::requireComponents(array(LOCALE_COMPONENT_PKP_SUBMISSION));
		
		$reviewReportDao =& DAORegistry::getDAO('ReviewReportDAO');
		list($commentsIterator, $reviewsIterator) = $reviewReportDao->getReviewReport($journal->getId());
		list($articlesIterator, $editorDecisionsReturner) = $reviewReportDao->getPapersReport($journal->getId());
		
		if(isset($_GET['Papers']))
		{
			header('content-disposition: attachment; filename=papers-' . date('Ymd') . '.csv');
			
			$decisions = array();
			$count_revisions = 0;
			while($row =& $editorDecisionsReturner->next())
			{	
				/*
				Accept Submission = 1
				Revisions Required = 2
				Resubmit for Review = 3
				Decline Submission = 4
				*/
				if($row['decision'] == '1' && !isset($decisions[$row['article_id']]['1']))
					$decisions[$row['article_id']]['1']['date_decided'] = $row['date_decided'];
				elseif($row['decision'] == '2')
					$decisions[$row['article_id']]['2'][] = array('date_decided' => $row['date_decided']);
				elseif($row['decision'] == '3' && !isset($decisions[$row['article_id']]['3']))
					$decisions[$row['article_id']]['3']['date_decided'] = $row['date_decided'];
				elseif($row['decision'] == '4' && !isset($decisions[$row['article_id']]['4']))
					$decisions[$row['article_id']]['4']['date_decided'] = $row['date_decided'];
			}

			$columns = array(
				'articleid' => 'Article ID',
				'article' => 'Article',
				'setting_value' => 'Section',
				'datesubmitted' => 'Date Article Submitted',
				'datepublished' => 'Date Article Published',
				'date_decided1' => 'Date Decision Accept Submission',
				'date_decided3' => 'Date Decision Resubmit for Review',
				'date_decided4' => 'Date Decision Decline Submission',
				'date_decided2_1' => 'Date Decision Revisions Required',
				'date_decided2_2' => 'Date Decision Revisions Required',
				'date_decided2_3' => 'Date Decision Revisions Required',
				'dateresubmit1' => 'Date Author Resubmit',
				'dateresubmit2' => 'Date Author Resubmit',
				'dateresubmit3' => 'Date Author Resubmit',
				'status' => 'Archived',
				'date_status_modified' => 'Date Article Archived',
			);

			$fp = fopen('php://output', 'wt');
			String::fputcsv($fp, array_values($columns));
	
			while($row =& $articlesIterator->next())
			{
				//print_r($row);
				foreach($columns as $index => $junk) 
				{
					$columns[$index] = $row[$index];
					
					if(isset($decisions[$row['articleid']]['1']['date_decided']))
						$columns['date_decided1'] = $decisions[$row['articleid']]['1']['date_decided'];
					
					if(isset($decisions[$row['articleid']]['3']['date_decided']))
						$columns['date_decided3'] = $decisions[$row['articleid']]['3']['date_decided'];
					
					if(isset($decisions[$row['articleid']]['4']['date_decided']))
						$columns['date_decided4'] = $decisions[$row['articleid']]['4']['date_decided'];
					
					if(isset($decisions[$row['articleid']]['2'][0]['date_decided']))
						$columns['date_decided2_1'] = $decisions[$row['articleid']]['2'][0]['date_decided'];
						
					if(isset($decisions[$row['articleid']]['2'][1]['date_decided']))
						$columns['date_decided2_2'] = $decisions[$row['articleid']]['2'][1]['date_decided'];
						
					if(isset($decisions[$row['articleid']]['2'][2]['date_decided']))
						$columns['date_decided2_3'] = $decisions[$row['articleid']]['2'][2]['date_decided'];
						
					$authorDecisionsReturner = $reviewReportDao->getPapersReportAuthors($row['articleid']);
					$author_decision = 1;
					while($row2 =& $authorDecisionsReturner->next())
					{
						//die(var_dump($row2));
						if(isset($row2['date_only']))
							if($author_decision)
							{
								$columns["dateresubmit$author_decision"] = $row2['date_only'];
								$author_decision++;
							}	
					}
				}
				//die(var_dump($columns));
				String::fputcsv($fp, $columns);
				unset($row);
			}
			fclose($fp);
		
		}
		else
		{
			header('content-disposition: attachment; filename=reviews-' . date('Ymd') . '.csv');
	
			$comments = array();
			while ($row =& $commentsIterator->next()) {
				if (isset($comments[$row['article_id']][$row['author_id']])) {
					$comments[$row['article_id']][$row['author_id']] .= "; " . $row['comments'];
				} else {
					$comments[$row['article_id']][$row['author_id']] = $row['comments'];
				}
			}
	
			$yesnoMessages = array( 0 => Locale::translate('common.no'), 1 => Locale::translate('common.yes'));
	
			import('classes.submission.reviewAssignment.ReviewAssignment');
			$recommendations = ReviewAssignment::getReviewerRecommendationOptions();
	
			$columns = array(
				'round' => Locale::translate('plugins.reports.reviews.round'),
				'article' => Locale::translate('article.articles'),
				'articleid' => Locale::translate('article.submissionId'),
				'reviewerid' => Locale::translate('plugins.reports.reviews.reviewerId'),
				'reviewer' => Locale::translate('plugins.reports.reviews.reviewer'),
				'firstname' => Locale::translate('user.firstName'),
				'middlename' => Locale::translate('user.middleName'),
				'lastname' => Locale::translate('user.lastName'),
				'dateassigned' => Locale::translate('plugins.reports.reviews.dateAssigned'),
				'datenotified' => Locale::translate('plugins.reports.reviews.dateNotified'),
				'dateconfirmed' => Locale::translate('plugins.reports.reviews.dateConfirmed'),
				'datecompleted' => Locale::translate('plugins.reports.reviews.dateCompleted'),
				'datereminded' => Locale::translate('plugins.reports.reviews.dateReminded'),
				'declined' => Locale::translate('submissions.declined'),
				'cancelled' => Locale::translate('common.cancelled'),
				'recommendation' => Locale::translate('reviewer.article.recommendation'),
				'comments' => Locale::translate('comments.commentsOnArticle')
			);
			$yesNoArray = array('declined', 'cancelled');
	
			$fp = fopen('php://output', 'wt');
			String::fputcsv($fp, array_values($columns));
	
			while ($row =& $reviewsIterator->next()) {
				foreach ($columns as $index => $junk) {
					if (in_array($index, $yesNoArray)) {
						$columns[$index] = $yesnoMessages[$row[$index]];
					} elseif ($index == "recommendation") {
						$columns[$index] = (!isset($row[$index])) ? Locale::translate('common.none') : Locale::translate($recommendations[$row[$index]]);
					} elseif ($index == "comments") {
						if (isset($comments[$row['articleid']][$row['reviewerid']])) {
							$columns[$index] = $comments[$row['articleid']][$row['reviewerid']];
						} else {
							$columns[$index] = "";
						}
					} else {
						$columns[$index] = $row[$index];
					}
				}
				String::fputcsv($fp, $columns);
				unset($row);
			}
			fclose($fp);
		}

		
	}
}

?>
