<?php

/**
 * @file ReviewReportDAO.inc.php
 *
 * Copyright (c) 2003-2011 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 * 
 * @class ReviewReportDAO
 * @ingroup plugins_reports_review
 * @see ReviewReportPlugin
 *
 * @brief Review report DAO
 */

//$Id$


import('classes.article.ArticleComment');
import('lib.pkp.classes.db.DBRowIterator');

class ReviewReportDAO extends DAO {
	/**
	 * Get the review report data.
	 * @param $journalId int
	 * @return array
	 */
	function getReviewReport($journalId) {
		$primaryLocale = Locale::getPrimaryLocale();
		$locale = Locale::getLocale();

		$result =& $this->retrieve(
			'SELECT	article_id,
				comments,
				author_id
			FROM	article_comments
			WHERE	comment_type = ?',
			array(
				COMMENT_TYPE_PEER_REVIEW
			)
		);
		import('lib.pkp.classes.db.DBRowIterator');
		$commentsReturner = new DBRowIterator($result);

		$result =& $this->retrieve(
			'SELECT r.round AS round,
				COALESCE(asl.setting_value, aspl.setting_value) AS article,
				a.article_id AS articleId,
				u.user_id AS reviewerId,
				u.username AS reviewer,
				u.first_name AS firstName,
				u.middle_name AS middleName,
				u.last_name AS lastName,
				r.date_assigned AS dateAssigned,
				r.date_notified AS dateNotified,
				r.date_confirmed AS dateConfirmed,
				r.date_completed AS dateCompleted,
				r.date_reminded AS dateReminded,
				(r.declined=1) AS declined,
				(r.cancelled=1) AS cancelled,
				r.recommendation AS recommendation
			FROM	review_assignments r
				LEFT JOIN articles a ON r.submission_id = a.article_id
				LEFT JOIN article_settings asl ON (a.article_id=asl.article_id AND asl.locale=? AND asl.setting_name=?)
				LEFT JOIN article_settings aspl ON (a.article_id=aspl.article_id AND aspl.locale=a.locale AND aspl.setting_name=?),
				users u
			WHERE	u.user_id=r.reviewer_id AND a.journal_id= ?
			ORDER BY article',
			array(
				$locale, // Article title
				'title',
				'title',
				$journalId
			)
		);
		$reviewsReturner = new DBRowIterator($result);

		return array($commentsReturner, $reviewsReturner);
	}
	
	
	
	function getPapersReport($journalId) {
		$primaryLocale = Locale::getPrimaryLocale();
		$locale = Locale::getLocale();
		
		import('lib.pkp.classes.db.DBRowIterator');

		$result =& $this->retrieve("
			SELECT 
				a.article_id AS articleId,
				a.date_submitted AS dateSubmitted,
				a.publish_online AS datePublished,
				a.date_status_modified AS date_status_modified,
				CASE WHEN a.status = '1' THEN 'No' WHEN a.status = '0' THEN 'Yes' END AS status,
				
				ael.date_sent as dateResubmit,
				
				COALESCE(asl.setting_value, aspl.setting_value) AS article,
				
				ss.setting_value
			FROM	
				articles a
			LEFT JOIN article_settings asl ON (a.article_id = asl.article_id AND asl.locale = ? AND asl.setting_name = ?)
			LEFT JOIN article_settings aspl ON (a.article_id = aspl.article_id AND aspl.locale = a.locale AND aspl.setting_name = ?)
			LEFT JOIN article_email_log ael ON a.article_id = ael.article_id
			LEFT JOIN section_settings ss ON a.section_id = ss.section_id
			WHERE 
				a.journal_id = ? 
				AND a.date_submitted <> ''
				AND ss.setting_name = ?
			GROUP BY 
				a.article_id
			",
			array(
				$locale,
				'title',
				'title',
				$journalId,
				'title',
			)
		);
		$articlesReturner = new DBRowIterator($result);
		
		$result =& $this->retrieve("
			SELECT	
				ed.*
			FROM 
				edit_decisions ed
			ORDER BY 
				edit_decision_id DESC
			"
		);
		$editorDecisionsReturner = new DBRowIterator($result);

		return array($articlesReturner, $editorDecisionsReturner);
	}
	
	
	function getPapersReportAuthors($articleId) {
		$primaryLocale = Locale::getPrimaryLocale();
		$locale = Locale::getLocale();
		
		import('lib.pkp.classes.db.DBRowIterator');
		
		$result =& $this->retrieve("
			SELECT	
				DATE(a.date_modified) date_only
			FROM 
				article_files a
			WHERE 
				a.article_id = ?
				AND a.type = 'submission/editor'
			GROUP BY 
				date_only
			ORDER BY 
				a.date_modified DESC
			LIMIT 3
			",
			array(
				$articleId
			)
		);
		$authorDecisionsReturner = new DBRowIterator($result);

		return $authorDecisionsReturner;
	}
	
	
}

?>
